#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
#include <sstream>
using std::string;
#include <set>
using std::set;


/*Shateesh
  resources used:
  -c++ tutorial on sets: https://www.youtube.com/watch?v=_RYLJoJBC7k
  -http://www.cplusplus.com/reference/cctype/isspace/
  time spent on project: ~7 hrs
*/

//Hyun Jun Nam
//resource: http://www.cppforschool.com/assignment/string-sol/word-count-string.html
//time spent: ~4 hrs

//Kelly Lu
//time spent: ~5 hrs

//Franklin Figueroa
//time spent: ~6.5 hrs

unsigned long uniqueLines(set<string>& ll){
  /*idea: in main, we stored input into a set of lines already. */

  return ll.size();
}

unsigned long uniqueWords(set<string>& wl){
  /*counting words */
  /*idea: for every element in the set, count the number of spaces.
    each element in the set is a line. */
  int x = 0;
  for(set<string>::iterator i = wl.begin(); i!=wl.end(); i++) {
    // get line out of wl and count words in *i
    string temp = (*i);  //got the line out of the set
    for(int c = 1; c <temp.size(); c++){
      if(isspace(temp[c]) && !(isspace(temp[c-1])))
        {x++;}
      }
    }
  return x;
  }

int words(string s)
{
    int num = 0;
    for(unsigned int i = 1; i < s.size();i++){
        if((isspace(s[i])  or i==s.size()-1) and !(isspace(s[i-1])))      // this checks for any kind of space or end of a line
            num++;
      }
    return num;
}
//the count starts from one and counts the spaces after the each word.


int main()
{
  set<string> text;
  string line;
  int linec = 0;
  int charcount = 0;
  int wordcount = 0;//added charcount and wordcount to declare the variable.
  while(getline(cin,line)){
    int b=words(line);
    text.insert(line);
    linec++;
    charcount += line.length()+1;  /* counts the characters in the given input*/
    wordcount += b;
    }
  {
    int d=uniqueLines(text);
    int e=uniqueWords(text);
    cout << linec << "  " << wordcount << " " << charcount << " " << d << " " << e << " " << endl;
    }

    return 0;
  }